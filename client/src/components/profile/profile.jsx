import * as React from 'react';
import { useSelector } from 'react-redux';
import ReactCrop from 'react-image-crop';
import { image as imageService } from 'src/services/services';
import { Grid, Image, Input, Button, Form } from 'src/components/common/common';
import { DEFAULT_USER_AVATAR } from 'src/common/constants/constants';
import { ButtonColor, ButtonType, IconName } from 'src/common/enums/enums';
import styles from './styles.module.scss';
import 'react-image-crop/dist/ReactCrop.css';

const Profile = () => {
  const [crop, setCrop] = React.useState({ unit: 'px',
    x: 130,
    y: 50,
    width: 200,
    height: 200,
    minWidth: 200,
    minHeight: 200,
    maxWidth: 200,
    maxHeight: 200,
    aspect: 1 / 1,
    locked: true
  });
  const { user } = useSelector(state => ({
    user: state.profile.user
  }));
  const [username, setName] = React.useState(user.username);
  const [image, setImage] = React.useState(user.image);
  const [isUploading, setIsUploading] = React.useState(false);
  const handleEditUser = async () => {
    // await onUserEdit({ id: user.id, imageId: image?.imageId || null, username });
  };
  const handleUploadFile = ({ target }) => {
    setIsUploading(true);
    const [file] = target.files;

    imageService.uploadImage(file)
      .then(({ id: imageId, link: imageLink }) => {
        setImage({ imageId, imageLink });
      })
      .catch(() => {
        // TODO: show error
      })
      .finally(() => {
        setIsUploading(false);
      });
  };
  const getCroppedImg = ({ imageN, cropN, fileName }) => {
    const canvas = document.createElement('canvas');
    const scaleX = imageN.naturalWidth / imageN.width;
    const scaleY = imageN.naturalHeight / imageN.height;
    canvas.width = cropN.width;
    canvas.height = cropN.height;
    const ctx = canvas.getContext('2d');

    ctx.drawImage(
      imageN,
      cropN.x * scaleX,
      cropN.y * scaleY,
      cropN.width * scaleX,
      cropN.height * scaleY,
      0,
      0,
      cropN.width,
      cropN.height
    );

    // As Base64 string
    // const base64Image = canvas.toDataURL('image/jpeg');

    // As a blob
    return new Promise(resolve => {
      canvas.toBlob(blob => {
        blob.name = fileName;
        resolve(blob);
      }, 'image/jpeg', 1);
    });
  };

  const handleCropImage = async () => {
    const resultImg = await getCroppedImg(image, crop, image.fileName);
    imageService.uploadImage(resultImg)
      .then(({ id: imageId, link: imageLink }) => {
        setImage({ imageId, imageLink });
      })
      .catch(() => {
        // TODO: show error
      })
      .finally(() => {
        setIsUploading(false);
      });
  };
  return (
    <Grid container textAlign="center" style={{ paddingTop: 30 }}>
      <Grid.Column>
        <Form onSubmit={handleEditUser}>
          <Image
            centered
            src={image?.imageLink || image?.link || DEFAULT_USER_AVATAR}
            size="medium"
            circular
          />
          {(image?.imageLink || image?.link)
            && <ReactCrop src={image?.imageLink || image?.link} crop={crop} onChange={newCrop => setCrop(newCrop)} />}
          <Button
            color="teal"
            onClick={handleCropImage}
          >
            OK
          </Button>
          <br />
          <Input
            icon="user"
            iconPosition="left"
            placeholder="Username"
            type="text"
            value={username}
            onChange={ev => setName(ev.target.value)}
          />
          <br />
          <br />
          <Input
            icon="at"
            iconPosition="left"
            placeholder="Email"
            type="email"
            disabled
            value={user.email}
          />
          <br />
          <br />
          <Button
            color="teal"
            isLoading={isUploading}
            isDisabled={isUploading}
            iconName={IconName.IMAGE}
          >
            <label className={styles.btnImgLabel}>
              Attach image
              <input
                name="image"
                type="file"
                onChange={handleUploadFile}
                hidden
              />
            </label>
          </Button>
          <Button color={ButtonColor.BLUE} type={ButtonType.SUBMIT}>
            Save
          </Button>
        </Form>
      </Grid.Column>
    </Grid>
  );
};
export default Profile;
