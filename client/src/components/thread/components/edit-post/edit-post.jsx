import * as React from 'react';
import PropTypes from 'prop-types';
import { editPostType } from 'src/common/prop-types/prop-types';
import { ButtonColor, ButtonType, IconName } from 'src/common/enums/enums';
import {
  Modal,
  Button,
  Form,
  Image,
  Icon,
  Label
} from 'src/components/common/common';

import styles from './styles.module.scss';

const EditPost = ({ post, close, onPostEdit, uploadImage, onDeletePost }) => {
  const { id } = post;
  const [body, setBody] = React.useState(post.body);
  const [image, setImage] = React.useState(post.image);
  const [isUploading, setIsUploading] = React.useState(false);
  // setBody(body);
  // setImage(image);
  const handleEditPost = async () => {
    await onPostEdit({ id, imageId: image?.imageId || null, body });
    close();
  };
  const handleDeleteImage = () => {
    setIsUploading(false);
    setImage(null);
  };
  const handleUploadFile = ({ target }) => {
    setIsUploading(true);
    const [file] = target.files;

    uploadImage(file)
      .then(({ id: imageId, link: imageLink }) => {
        setImage({ imageId, imageLink });
      })
      .catch(() => {
        // TODO: show error
      })
      .finally(() => {
        setIsUploading(false);
      });
  };
  const handleDeletePost = () => {
    onDeletePost(id);
    close();
  };

  return (
    <Modal open onClose={close}>
      <Modal.Header className={styles.header}>
        <span>Edit Post</span>
      </Modal.Header>
      <Modal.Content>
        <Form onSubmit={handleEditPost}>
          <Form.TextArea
            name="body"
            value={body}
            placeholder="What is the news?"
            onChange={ev => setBody(ev.target.value)}
          />
          {(image?.imageLink || image?.link) && (
            <div className={styles.imageWrapper}>
              <Image
                className={styles.image}
                src={image?.imageLink || image?.link}
                alt="post"
              />
              <Label
                basic
                size="small"
                as="a"
                className={styles.toolbarBtn}
                onClick={handleDeleteImage}
              >
                <Icon name={IconName.CLOSE} />
              </Label>
            </div>
          )}
          <div className={styles.btnWrapper}>
            <Button
              color="teal"
              isLoading={isUploading}
              isDisabled={isUploading}
              iconName={IconName.IMAGE}
            >
              <label className={styles.btnImgLabel}>
                Attach image
                <input
                  name="image"
                  type="file"
                  onChange={handleUploadFile}
                  hidden
                />
              </label>
            </Button>
            <div>
              <Button color={ButtonColor.RED} onClick={handleDeletePost}>
                Delete
              </Button>
              <Button color={ButtonColor.BLUE} type={ButtonType.SUBMIT}>
                Post
              </Button>
            </div>
          </div>
        </Form>
      </Modal.Content>
    </Modal>
  );
};

EditPost.propTypes = {
  post: editPostType.isRequired,
  close: PropTypes.func.isRequired,
  onPostEdit: PropTypes.func.isRequired,
  uploadImage: PropTypes.func.isRequired,
  onDeletePost: PropTypes.func.isRequired
};

export default EditPost;
