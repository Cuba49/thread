import * as React from 'react';
import PropTypes from 'prop-types';
import { getFromNowTime } from 'src/helpers/helpers';
import { IconName } from 'src/common/enums/enums';
import { postType } from 'src/common/prop-types/prop-types';
import { Icon, Card, Image, Label } from 'src/components/common/common';

import { DEFAULT_USER_AVATAR } from 'src/common/constants/constants';

import styles from './styles.module.scss';

const Post = ({
  post,
  onPostLike,
  onPostDislike,
  onExpandedPostToggle,
  sharePost,
  editPost,
  currentUser
}) => {
  const {
    id,
    image,
    body,
    user,
    // likeCount,
    // dislikeCount,
    commentCount,
    createdAt,
    postReactions
  } = post;
  const likers = postReactions.filter(react => react.isLike);
  const dislikers = postReactions.filter(react => !react.isLike);
  const date = getFromNowTime(createdAt);
  const styleLike = postReactions.some(
    reaction => reaction.userId === currentUser && reaction.isLike === true
  )
    ? { color: '#1e70bf' }
    : {};
  const styleDislike = postReactions.some(
    reaction => reaction.userId === currentUser && reaction.isLike === false
  )
    ? { color: '#1e70bf' }
    : {};

  const handlePostLike = () => onPostLike(id, currentUser);
  const handlePostDislike = () => onPostDislike(id, currentUser);
  const handleExpandedPostToggle = () => onExpandedPostToggle(id);
  return (
    <Card style={{ width: '100%' }}>
      {image && <Image src={image.link} wrapped ui={false} />}
      <Card.Content>
        <Card.Meta>
          <span className="date">
            posted by
            {user.username}
            {' - '}
            {date}
          </span>
        </Card.Meta>
        <Card.Description>{body}</Card.Description>
        {JSON.stringify(postReactions)}
      </Card.Content>
      <Card.Content extra>
        <Label
          basic
          size="small"
          as="a"
          className={styles.likes}
          onClick={handlePostLike}
          style={styleLike}
        >
          <Icon name={IconName.THUMBS_UP} />
          {likers.length}
          <div className={styles.likers}>
            {likers.slice(0, 5).map(react => (
              <Image
                className={styles.userImage}
                title={react.user?.username}
                circular
                width="25"
                height="25"
                src={react.user?.image?.link ?? DEFAULT_USER_AVATAR}
              />
            ))}
          </div>
        </Label>
        <Label
          basic
          size="small"
          as="a"
          className={styles.toolbarBtn}
          onClick={handlePostDislike}
          style={styleDislike}
        >
          <Icon name={IconName.THUMBS_DOWN} />
          {dislikers.length}
        </Label>
        <Label
          basic
          size="small"
          as="a"
          className={styles.toolbarBtn}
          onClick={handleExpandedPostToggle}
        >
          <Icon name={IconName.COMMENT} />
          {commentCount}
        </Label>
        <Label
          basic
          size="small"
          as="a"
          className={styles.toolbarBtn}
          onClick={() => sharePost(id)}
        >
          <Icon name={IconName.SHARE_ALTERNATE} />
        </Label>
        {currentUser === post.user.id && (
          <Label
            basic
            size="small"
            as="a"
            className={styles.toolbarBtn}
            onClick={() => editPost({ id, image, body })}
          >
            <Icon name={IconName.EDIT} />
          </Label>
        )}
      </Card.Content>
    </Card>
  );
};

Post.propTypes = {
  post: postType.isRequired,
  onPostLike: PropTypes.func.isRequired,
  onPostDislike: PropTypes.func.isRequired,
  onExpandedPostToggle: PropTypes.func.isRequired,
  sharePost: PropTypes.func.isRequired,
  editPost: PropTypes.func.isRequired,
  currentUser: PropTypes.string.isRequired
};

export default Post;
